import sys
from database import edit_iris, get_connection, get_null_area_iris, init_schema_and_table, load_data

# Parameters
database_credential = 'host=91.121.117.24 dbname=pdm_test user=pdm password=...'
if database_credential == 'host=91.121.117.24 dbname=pdm_test user=pdm password=...':
    sys.exit("Do not forget to modify the credential...")
schema = 'vsasyan'
table = 'iris'

# Get connection
conn = get_connection(database_credential)
# Init the schema and the table
need_to_load_data = init_schema_and_table(conn, schema, table)
# If needed load the data
if need_to_load_data:
    csv_file = 'iris.csv' # Relative or absolute
    load_data(conn, schema, table, csv_file)
# Complete rows with null columns
get_null_area_iris(conn, schema, table)