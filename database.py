import pandas
import psycopg2
import psycopg2.extras

def init_schema_and_table(database_conn, schema, table):
    """We test if the schema and the table exist:
    - if needed we create the schema and/or the table and return True
    - else we return False
    
    Arguments:
        database_conn {str} -- Connection to the database
        schema {str} -- Name of the schema
        table {str} -- Name of the table
    
    Returns:
        [bool] -- False if the schema and the table were already created
    """
    print('init schema and table...')

    # Declaration of the needed SQL requests
    sql_select_schema = """
        SELECT 1
        FROM information_schema.schemata
        WHERE schema_name = '{schema}'
        ;
    """
    sql_create_schema = """
        CREATE SCHEMA {schema};
    """
    sql_select_table = """
        SELECT 1
        FROM   information_schema.tables 
        WHERE  table_schema = '{schema}'
        AND    table_name = '{table}'
        ;
    """
    sql_create_table = """
        CREATE TABLE {schema}.{table}(
            id SERIAL,
            sepal_length REAL,
            sepal_width REAL,
            petal_length REAL,
            petal_width REAL,
            sepal_area REAL,
            petal_area REAL,
            name TEXT
        );
    """

    # Have we done modification ?
    modification = False

    # Get a cursor
    with database_conn.cursor() as cur:
        # Test if schema exists
        cur.execute(sql_select_schema.format(schema=schema))
        data = cur.fetchone()
        if data == None:
            # So we have done modification
            modification = True
            # Schema does not exist, create it !
            cur.execute(sql_create_schema.format(schema=schema))
            # Then commit modification
            database_conn.commit()

        # Test if table exists
        cur.execute(sql_select_table.format(schema=schema, table=table))
        data = cur.fetchone()
        if data == None:
            # So we have done modification
            modification = True
            # Table does not exist, create it !
            cur.execute(sql_create_table.format(schema=schema, table=table))
            # Then commit modification
            database_conn.commit()

    # Print status
    print('init schema and table OK ({})'.format(modification))

    # Return the boolean modification
    return modification


def load_data(database_conn, schema, table, csv_file):
    """This function load the csv_file inside of the database
    
    Arguments:
        database_conn {str} -- Connection to the database
        schema {str} -- Name of the schema
        table {str} -- Name of the table
        csv_file {str} -- Path to the CSV file (relative or absolute)
    """
    # SQL request
    sql_insert_into = """
        INSERT INTO {schema}.{table}(
            sepal_length,
            sepal_width,
            petal_length,
            petal_width,
            name
        ) VALUES(%s, %s, %s, %s, %s);
    """
    print("Data importation...")

    # Open the CSV file with pandas
    data = pandas.read_csv(csv_file)

    # Get a cursor
    with database_conn.cursor() as cur:

        # Iterable throw the dataframe to insert each line into the table
        for index, row in data.iterrows():
            # Create a tuple with the data to insert
            values = (row['sepal_length'], row['sepal_width'], row['petal_length'], row['petal_width'], row['name'])
            # Execute the SQL request, completed by the values
            cur.execute(sql_insert_into.format(schema=schema, table=table), values)
        
        # At the end, we commit all the results
        database_conn.commit()

    print("Data was imported in the table !")


def get_null_area_iris(database_conn, schema, table):
    """This function get all rows with null data
    
    Arguments:
        database_conn {str} -- Connection to the database
        schema {str} -- Name of the schema
        table {str} -- Name of the table
    """
    # SQL request
    sql_select = """
        SELECT * FROM {schema}.{table} WHERE sepal_area IS NULL OR petal_area IS NULL;
    """
    print('get null area')

    # Get a cursor
    with database_conn.cursor() as cur:
        # Select the rows with null data        
        cur.execute(sql_select.format(schema=schema, table=table))

        # Fetch the data
        rows = cur.fetchall()
    
    # For all rows, we can execute edit iris
    for iris in rows:
        edit_iris(database_conn, schema, table, dict(iris)) 
    print('get null area OK ({})'.format(len(rows)))


def edit_iris(database_conn, schema, table, iris):
    """This function get all rows with null data
    
    Arguments:
        database_conn {str} -- Connection to the database
        schema {str} -- Name of the schema
        table {str} -- Name of the table
        iris {dict} -- Dictionary with all iris informations (id, petal_width, petal_length, ...)
    """
    # SQL request
    sql_update = """
        UPDATE {schema}.{table} SET sepal_area = %s, petal_area = %s WHERE id = %s;
    """

    # Get a cursor
    with database_conn.cursor() as cur:
        # Calculate the areas
        sepal_area = iris['sepal_length'] * iris['sepal_width']
        petal_area = iris['petal_length'] * iris['petal_width']
        # Create a tuple with the data to update
        values = (sepal_area, petal_area, iris['id'])
        # Execute the SQL request, completed by the values
        cur.execute(sql_update.format(schema=schema, table=table), values)
        # Commit
        database_conn.commit()


def get_connection(database_credential):
    """Create a connection to the database
    
    Arguments:
        database_credential {str} -- Credentials to connect to the database
    
    Returns:
        [Connection] -- A connection to the database
    """
    conn = psycopg2.connect(database_credential, cursor_factory=psycopg2.extras.RealDictCursor)
    return conn


if __name__ == '__main__':
    # Here you can test the function when the script it is run directly (not imported)
    # Parameters
    database_credential = 'host=127.0.0.1 dbname=test user=postgres password=postgres'
    schema = 'vsasyan'
    table = 'iris'
    # Get connection
    conn = get_connection(database_credential)
    # Init the schema and the table
    need_to_load_data = init_schema_and_table(conn, schema, table)
    # If needed load the data
    if need_to_load_data:
        csv_file = 'iris.csv' # Relative or absolute
        load_data(conn, schema, table, csv_file)
    # Complete rows with null columns
    get_null_area_iris(conn, schema, table)